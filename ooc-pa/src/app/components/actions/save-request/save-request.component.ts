import { Component, OnInit,Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
@Component({
  selector: 'app-save-request',
  templateUrl: './save-request.component.html',
  styleUrls: ['./save-request.component.css']
})
export class SaveRequestComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  @Input() parentForm: FormGroup;
}
