import { Component, OnInit,Input } from '@angular/core';
import { FormGroup, FormControl,FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-surgical-procedure-form',
  templateUrl: './surgical-procedure-form.component.html',
  styleUrls: ['./surgical-procedure-form.component.css']
})
export class SurgicalProcedureFormComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  @Input() parentForm: FormGroup;
}
