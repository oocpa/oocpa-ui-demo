import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurgicalProcedureFormComponent } from './surgical-procedure-form.component';

describe('SurgicalProcedureFormComponent', () => {
  let component: SurgicalProcedureFormComponent;
  let fixture: ComponentFixture<SurgicalProcedureFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurgicalProcedureFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurgicalProcedureFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
