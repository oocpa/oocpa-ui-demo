import { Component, OnInit,Input } from '@angular/core';
import { FormGroup, FormControl,FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-cancer-form',
  templateUrl: './cancer-form.component.html',
  styleUrls: ['./cancer-form.component.css']
})
export class CancerFormComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  private selectedLink: string="New";     
    

  setradio(e: string): void {
      this.selectedLink = e;  
  }  
  
  
  isSelected(name: string): boolean {  
  
      if (!this.selectedLink) { // if no radio button is selected, always return false so every nothing is shown  
          return false;  
      }    
  
      return (this.selectedLink === name); // if current radio button is selected, return true, else return false  
  }  
  @Input() parentForm: FormGroup;
}
