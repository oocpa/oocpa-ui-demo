import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancerFormComponent } from './cancer-form.component';

describe('CancerFormComponent', () => {
  let component: CancerFormComponent;
  let fixture: ComponentFixture<CancerFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancerFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
