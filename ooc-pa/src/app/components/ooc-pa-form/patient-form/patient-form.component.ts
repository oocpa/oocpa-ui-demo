import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-patient-form',
  templateUrl: './patient-form.component.html',
  styleUrls: ['./patient-form.component.css']
})
export class PatientFormComponent implements OnInit {

  constructor() { }

  ngOnInit() {
 
  }
  private selectedLink: string="New";     

  setradio(e: string): void {
    this.selectedLink = e;  
}  

isSelected(name: string): boolean {  
  if(this.selectedLink == 'inputOtherComplete')
  {
   this.parentForm.patchValue({formComplete: ''});
   
  }
  if (!this.selectedLink) { // if no radio button is selected, always return false so every nothing is shown  
      return false;  
  }    

  return (this.selectedLink === name); // if current radio button is selected, return true, else return false  
}  



@Input() parentForm: FormGroup;


}
