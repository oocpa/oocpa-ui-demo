import { Component, OnInit,Input } from '@angular/core';
import { FormGroup, FormControl,FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-proposedooc-form',
  templateUrl: './proposedooc-form.component.html',
  styleUrls: ['./proposedooc-form.component.css']
})
export class ProposedoocFormComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input() parentForm: FormGroup;
}
