import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProposedoocFormComponent } from './proposedooc-form.component';

describe('ProposedoocFormComponent', () => {
  let component: ProposedoocFormComponent;
  let fixture: ComponentFixture<ProposedoocFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProposedoocFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProposedoocFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
