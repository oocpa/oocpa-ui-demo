import { Component, OnInit,Input } from '@angular/core';
import { FormGroup, FormControl,FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-inpatient-residential-form',
  templateUrl: './inpatient-residential-form.component.html',
  styleUrls: ['./inpatient-residential-form.component.css']
})
export class InpatientResidentialFormComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  @Input() parentForm: FormGroup;
}
