import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InpatientResidentialFormComponent } from './inpatient-residential-form.component';

describe('InpatientResidentialFormComponent', () => {
  let component: InpatientResidentialFormComponent;
  let fixture: ComponentFixture<InpatientResidentialFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InpatientResidentialFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InpatientResidentialFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
