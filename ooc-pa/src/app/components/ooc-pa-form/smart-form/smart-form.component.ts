import { Component, OnInit } from '@angular/core';

import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { format } from 'url';
import { Dao } from 'src/app/dao/dao';
@Component({
  selector: 'app-smart-form',
  templateUrl: './smart-form.component.html',
  styleUrls: ['./smart-form.component.css']
})

export class SmartFormComponent implements OnInit {
  parentForm: FormGroup;
  saveClicked = false;
  constructor(private formBuilder: FormBuilder,private http: HttpClient) { //this.parentForm = this.createFormGroup();
  }

  ngOnInit() {
    this.parentForm = new FormGroup({


      // Work related heater
      workRelated: new FormControl(),

      // Service Header
      treatment: new FormControl(),


      // Patient Form Start

      lastNameControl: new FormControl('', Validators.required),
      
      firstNameControl: new FormControl('', Validators.required),
      intialControl: new FormControl('', Validators.required),
      dobInput: new FormControl('', Validators.required),
      inputSex: new FormControl('', Validators.required),
      healthNumber: new FormControl('', Validators.required),
      versionCode: new FormControl('', Validators.required),
      inputCity: new FormControl('', Validators.required),
      inputProvince: new FormControl('', Validators.required),
      inputPostal: new FormControl('', Validators.required),

      inputHomePhone: new FormControl('', [Validators.required, Validators.minLength(10)]),

      inputDayPhone: new FormControl('', Validators.minLength(10)),
      inputExtension: new FormControl(),
      guardianlastName: new FormControl(),
      guardianfirstName: new FormControl(),

      formComplete: new FormControl('',Validators.required),


      // Reffering Phys
      physLastName:  new FormControl('', Validators.required),
      physFirstName:  new FormControl('', Validators.required),

      physInputCity:  new FormControl('', Validators.required),
      physInputProvince:  new FormControl('', Validators.required),
      physInputPostal:  new FormControl('', Validators.required),
      physProviderBillingNumber:  new FormControl('', Validators.required),
      physInputPhone: new FormControl('', [Validators.required, Validators.minLength(10)]),
      physInputExtension:new FormControl('', Validators.required),
      physInputFax:new FormControl('', [Validators.required, Validators.minLength(10)]),
      physEmail: new FormControl(),

      //Proposed OOC Health Facility/Hospital
      propFaculity: new FormControl('', Validators.required),
      propAddress: new FormControl('', Validators.required),
      propCity: new FormControl('', Validators.required),
      propStateCountry: new FormControl('', Validators.required),
      propCode: new FormControl('', Validators.required),
      propOOC: new FormControl('', Validators.required),
      propLastName: new FormControl('', Validators.required),
      propFirstName: new FormControl('', Validators.required),
      propInputPhone: new FormControl('', [Validators.required, Validators.minLength(10)]),
      propInputExtension: new FormControl('', Validators.required),
      propInputFax: new FormControl('', [Validators.required, Validators.minLength(10)]),
      propEmail: new FormControl(),

      //Treatment - General Information
      treatmentInputDiagnosis: new FormControl(),
      treatmentInputCode: new FormControl(),
      treatmentServiceType: new FormControl(),
      treatmentHosptialStay: new FormControl(),
      treatmentAdmissionDate: new FormControl(),
      treatmentOOCConsult: new FormControl(),
      treatmentSurgeryDate: new FormControl(),
      treatmentProposedTreatment: new FormControl(),
      treatmentOutOfCountry: new FormControl(),
      treatmentPreviousRequest: new FormControl(),
      treatmentCardiacCare: new FormControl(),


      //Cancer Treatment Requested
      cancerTreatmentSelector: new FormControl(),
      cancerNamesOfPhysicans: new FormControl(),
      cancerDocumentation: new FormControl(),


      //Inpatient Residential Treatment Requested
      inpatientSubstanceMentalHealth: new FormControl(),
      inpatientEating: new FormControl(),
      inpatientConsulted: new FormControl(),
      inpatientConsultedFiles: new FormControl(),

      //Surgical Procedure Requested
      surgicalConsulted: new FormControl(),
      surgicalConsultedFile: new FormControl(),

      //Bariatric Surgery - Treatment Requested
      bariatrcTreatment: new FormControl(),
      bariatricProcedure: new FormControl(),
      bariaticFt: new FormControl(),
      bariaticIn: new FormControl(),
      bariaticCM: new FormControl(),
      bariaticlb: new FormControl(),
      bariaticKg: new FormControl(),
      bariatricMorbid: new FormControl(),
      bariatricMorbidFile: new FormControl(),
      bariatricCondtion: new FormControl(),
      bariatricConsulted: new FormControl(),
      bariatricConsultedFile: new FormControl(),

      //MRI (Magentic Resonance Imaging) Requested
      mriContrast: new FormControl(),
      mriOpen: new FormControl(),
      mriOpenInformation: new FormControl(),
      mriFt: new FormControl(),
      mriIn: new FormControl(),
      mriCm: new FormControl(),
      mriLb: new FormControl(),
      mriKg: new FormControl(),
      mriAdnormalIn: new FormControl(),
      mriAdnormalCm: new FormControl(),

      //MRI (Magentic Resonance Imaging) Requested
      treatmentAppropriate: new FormControl(),
      treatmentEquivalente: new FormControl(),
      treatmentPerformed: new FormControl(),
      resultInDeath: new FormControl(),
      treatmentTissueDamage: new FormControl(),
      treatmentRequired: new FormControl(),
      treatmentDamageDelay: new FormControl(),
      treatmentPhysican: new FormControl(),
      treatmentLength: new FormControl(),
      treatmentAccepted: new FormControl(),
      treatmentExperminatal: new FormControl(),
      treatmentEquivalente2: new FormControl(),
      treatmentDetails: new FormControl(),
      treatmentFollowUp: new FormControl(),
    }, { updateOn: 'blur' });

  }

  private selectedLink: string = "New";
  private selectedLinkRequired: string = "New";

  setradio(e: string): void {

    this.selectedLink = e;
  }


  isSelected(name: string): boolean {



    if (!this.selectedLink) { // if no radio button is selected, always return false so every nothing is shown  
      return false;
    }

    return (this.selectedLink === name); // if current radio button is selected, return true, else return false  
  }

  setradioRequired(e: string): void {

    this.selectedLinkRequired = e;
  }


  isSelectedRequired(name: string): boolean {



    if (!this.selectedLinkRequired) { // if no radio button is selected, always return false so every nothing is shown  
      return false;
    }

    return (this.selectedLinkRequired === name); // if current radio button is selected, return true, else return false  
  }




onSubmit(buttonType): void {
  if(this.parentForm.valid == false)
  {
    this.scrollIfFormHasErrors(this.parentForm);
    this.validateAllFormFields(this.parentForm);
    
  //alert("Unable to submit, form is not complete")
  }
  else{
  const dao = new Dao(this.http);
   if(this.saveClicked) {
    dao.dataBind(this.parentForm);
    dao.saveForm(this.parentForm);
  }
  else{
   
    dao.dataBind(this.parentForm);
    dao.submitNewForm(this.parentForm);
  }
}
}

// variable to know if Save is clicked
public onSubmitClick(): void {
  this.saveClicked = false;
}
// variable to know if Save is not clicked
public onSaveClick(): void {
  this.saveClicked = true;
}


  
  revert() {
    // Resets to blank object
    this.parentForm.reset();
  
    // Resets to provided model

  }


  scrollTo(error: Element): void {
    if(error) { 
      error.scrollIntoView({ behavior: 'smooth' });
    }
 }
 
 scrollToError(): void {
    const firstError = document.querySelector('.ng-invalid');
    this.scrollTo(firstError);
 }
 
 async scrollIfFormHasErrors(form: FormGroup): Promise <any> {
   await form.invalid;
   this.scrollToError();
 }

 validateAllFormFields(form: FormGroup) {         
  Object.keys(form.controls).forEach(field => {  
    const control = form.get(field);             
    if (control instanceof FormControl) {            
      control.markAsTouched({ onlySelf: true });
    } else if (control instanceof FormGroup) {        
      this.validateAllFormFields(control);            
    }
  });
}

}