import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MriFormComponent } from './mri-form.component';

describe('MriFormComponent', () => {
  let component: MriFormComponent;
  let fixture: ComponentFixture<MriFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MriFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MriFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
