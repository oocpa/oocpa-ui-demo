import { Component, OnInit,Input } from '@angular/core';
import { FormGroup, FormControl,FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-mri-form',
  templateUrl: './mri-form.component.html',
  styleUrls: ['./mri-form.component.css']
})
export class MriFormComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  @Input() parentForm: FormGroup;
}
