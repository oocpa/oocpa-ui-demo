import { Component, OnInit,Input } from '@angular/core';
import { FormGroup, FormControl,FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-treatment-availability-form',
  templateUrl: './treatment-availability-form.component.html',
  styleUrls: ['./treatment-availability-form.component.css']
})
export class TreatmentAvailabilityFormComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  @Input() parentForm: FormGroup;
}
