import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreatmentAvailabilityFormComponent } from './treatment-availability-form.component';

describe('TreatmentAvailabilityFormComponent', () => {
  let component: TreatmentAvailabilityFormComponent;
  let fixture: ComponentFixture<TreatmentAvailabilityFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreatmentAvailabilityFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreatmentAvailabilityFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
