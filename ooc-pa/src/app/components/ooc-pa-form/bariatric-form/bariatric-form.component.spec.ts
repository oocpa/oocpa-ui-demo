import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BariatricFormComponent } from './bariatric-form.component';

describe('BariatricFormComponent', () => {
  let component: BariatricFormComponent;
  let fixture: ComponentFixture<BariatricFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BariatricFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BariatricFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
