import { Component, OnInit,Input } from '@angular/core';
import { FormGroup, FormControl,FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-bariatric-form',
  templateUrl: './bariatric-form.component.html',
  styleUrls: ['./bariatric-form.component.css']
})
export class BariatricFormComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  @Input() parentForm: FormGroup;
}
