import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefferingOntarioPhysFormComponent } from './reffering-ontario-phys-form.component';

describe('RefferingOntarioPhysFormComponent', () => {
  let component: RefferingOntarioPhysFormComponent;
  let fixture: ComponentFixture<RefferingOntarioPhysFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefferingOntarioPhysFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefferingOntarioPhysFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
