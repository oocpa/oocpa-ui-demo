import { Component, OnInit,Input } from '@angular/core';
import { FormGroup, FormControl,FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-reffering-ontario-phys-form',
  templateUrl: './reffering-ontario-phys-form.component.html',
  styleUrls: ['./reffering-ontario-phys-form.component.css']
})
export class RefferingOntarioPhysFormComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input() parentForm: FormGroup;
}
