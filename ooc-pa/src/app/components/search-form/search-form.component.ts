import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Dao } from 'src/app/dao/dao';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})

export class SearchFormComponent implements OnInit {
  searchForm: FormGroup;
  results : any;
  myResults: any[];
  saveClicked = false;
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.onSaveClick()
    this.searchForm = new FormGroup({

      // Service Header
      formID: new FormControl(),
      formResponse: new FormControl()

    }, { updateOn: 'blur' });
  }

// variable to know if Save is not clicked
public onSaveClick(): void {
  this.saveClicked = true;
  this.onSubmit()
}
  onSubmit(): void {
    
    const dao = new Dao(this.http);
  ;

 
 //dao.searchForm(this.searchForm);
 dao.getAllData().subscribe((data)=> {
  this.myResults = JSON.parse(JSON.stringify(data))
  
},error => {
  console.log('error: ', error)
});
   
 
   
    }

   
  
}
  

