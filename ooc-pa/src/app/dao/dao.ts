import { FormGroup } from '@angular/forms';
import { Patient } from 'src/app/model/patient/patient';
import { Provider } from 'src/app/model/provider/provider';
import { Proposed } from 'src/app/model/proposed/proposed';
import { Treatment } from 'src/app/model/treatment/treatment';
import { HttpClient } from '@angular/common/http';
import { analyzeAndValidateNgModules } from '@angular/compiler';
export class Dao {
    patient = new Patient();
    provider = new Provider();
    proposed = new Proposed();
    treatment = new Treatment();

    response:any;
    constructor(private http: HttpClient) {
        this.getAllData()
        .subscribe(data => this.response = data);
    }

    dataBind(formData: FormGroup) {
        this.bindPatient(formData);
        this.bindProvider(formData);
        this.bindProposed(formData);
        this.bindTreatment(formData);
        console.log(JSON.stringify(this.patient), JSON.stringify(this.provider), JSON.stringify(this.proposed));
    }

    bindPatient(formData: FormGroup) {

        this.patient.contrtructPaitent(formData);

    }

    bindProvider(formData: FormGroup) {
        this.provider.contructProvider(formData);
    }

    bindProposed(formData: FormGroup)
    {
        this.proposed.contrtructProposed(formData);
    }

    bindTreatment(formData: FormGroup)
    {
        this.treatment.contructTreatment(formData);
    }

    submitNewForm(formData: FormGroup) {
        this.submitOOCPAWorkRelated(formData);
    }

    alertform(alert)
    {
        alert(alert);
    }
    submitOOCPAWorkRelated(formData: FormGroup) {
        // OOCPA Request: {"id":1,"workRelated":false,"created":"Oct 25, 2019, 9:52:17 AM","updated":"Oct 25, 2019, 9:52:17 AM"}
        //http://localhost:3000/submit nodeJS
        const req = this.http.post('http://localhost:3000/submit', {
            id: '',
            workRelated: formData.controls['workRelated'].value,
            patient: this.patient,
            provider: this.provider,
            proposed: this.proposed,
            treatment: this.treatment

            //  JSON.stringify(this.parentForm.value)

        })
            .subscribe(
                res => {
                    console.log(res);
                },
                err => {
                    console.log("Error occured");
                }
            );

            req.unsubscribe;
    }

    saveForm(formData: FormGroup) {

        const req = this.http.post('http://localhost:8380/api/appreq/save', {
            patient: this.patient,
            provider: this.provider,
            proposed: this.proposed

          
        })
            .subscribe(
                res => {
                    console.log(res);
                },
                err => {
                    console.log("Error occured");
                }
            );

            req.unsubscribe;
    }

    searchForm(formData: FormGroup) {

        const urlString = "http://localhost:3000/search";
     
        const req = this.http.get(urlString, {


        })
          .subscribe(
                res => {
                  
             
                   console.log(JSON.stringify(res));
                  
                   
                 //  this.response = res;
                  
                   
                },
                err => {
              console.log("Error occured");
                
                }
            );
         
            req.unsubscribe
    }


    getAllData() {
        return this.http.get('http://localhost:3000/search/all');
    
      }
}