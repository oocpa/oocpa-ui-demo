import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule }   from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { SmartFormComponent } from './components/ooc-pa-form/smart-form/smart-form.component';
import { CancerFormComponent } from './components/ooc-pa-form/cancer-form/cancer-form.component';
import { RouterModule, Routes } from '@angular/router';

import { TopBarComponent } from './components/general/top-bar/top-bar.component';
import { FooterBarComponent } from './components/general/footer-bar/footer-bar.component';
import { PatientFormComponent } from './components/ooc-pa-form/patient-form/patient-form.component';
import { RefferingOntarioPhysFormComponent } from './components/ooc-pa-form/reffering-ontario-phys-form/reffering-ontario-phys-form.component';
import { ProposedoocFormComponent } from './components/ooc-pa-form/proposedooc-form/proposedooc-form.component';
import { TreatmentFormComponent } from './components/ooc-pa-form/treatment-form/treatment-form.component';
import { InpatientResidentialFormComponent } from './components/ooc-pa-form/inpatient-residential-form/inpatient-residential-form.component';
import { SurgicalProcedureFormComponent } from './components/ooc-pa-form/surgical-procedure-form/surgical-procedure-form.component';
import { BariatricFormComponent } from './components/ooc-pa-form/bariatric-form/bariatric-form.component';
import { MriFormComponent } from './components/ooc-pa-form/mri-form/mri-form.component';
import { TreatmentAvailabilityFormComponent } from './components/ooc-pa-form/treatment-availability-form/treatment-availability-form.component';
import { SubmitRequestComponent } from './components/actions/submit-request/submit-request.component';
import { SaveRequestComponent } from './components/actions/save-request/save-request.component';
import { DisplayFormComponent } from './view/display-form/display-form.component';
import { HomeComponent } from './view/home/home.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchRequestComponent } from './components/actions/search-request/search-request.component';
import { DisplaySearchComponent } from './view/display-search/display-search.component';
import { DisplayNotFoundComponent } from './view/display-not-found/display-not-found.component';
import { HttpClientModule } from '@angular/common/http';
import {MatExpansionModule, MatInputModule,MatProgressSpinnerModule} from '@angular/material'
const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: '',   redirectTo: '/home', pathMatch: 'full' },
  { path: 'search-form', component: SearchFormComponent },
  { path: 'ooc-pa-form', component: DisplayFormComponent },
  { path: '**', component: DisplayNotFoundComponent }


];
@NgModule({
  declarations: [
    AppComponent,
    SmartFormComponent,
    CancerFormComponent,
    TopBarComponent,
    FooterBarComponent,
    PatientFormComponent,
    RefferingOntarioPhysFormComponent,
    ProposedoocFormComponent,
    TreatmentFormComponent,
    InpatientResidentialFormComponent,
    SurgicalProcedureFormComponent,
    BariatricFormComponent,
    MriFormComponent,
    TreatmentAvailabilityFormComponent,
    SubmitRequestComponent,
    SaveRequestComponent,
    DisplayFormComponent,
    HomeComponent,
    SearchFormComponent,
    SearchRequestComponent,
    DisplaySearchComponent,
    DisplayNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    MatExpansionModule, MatInputModule,MatProgressSpinnerModule,
    HttpClientModule,
    TypeaheadModule.forRoot(),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
