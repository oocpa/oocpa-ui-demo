import { Name } from '../utility/name';
import { Contact } from '../utility/contact';
import {FormGroup} from '@angular/forms';
export class Provider {
    stakeholderNo: string;
    firstName: Name;
    lastName: Name;
    primaryNumber: Contact;
    workNumber: Contact;
    extension: Contact;
    fax: Contact;
    email:Contact;

    constructor(options: {
        stakeholderNo?: string,
        firstName?: Name,
        lastName?: Name,
        primaryNumber?: Contact,
        workPhone?: Contact,
        extension?: Contact,
        fax?: Contact,
        email?: Contact,
    } = {}) {
        this.stakeholderNo = options.stakeholderNo;
        this.firstName = options.firstName;
        this.lastName = options.lastName;
        this.primaryNumber = options.primaryNumber;
        this.extension = options.extension;
        this.fax = options.fax;
        this.email = options.email;
    }

    contructProvider(formData: FormGroup)
    {
        /*
         physLastName: new FormControl(),
      physFirstName: new FormControl(),

      physInputCity: new FormControl(),
      physInputProvince: new FormControl(),
      physInputPostal: new FormControl(),
      physProviderBillingNumber: new FormControl(),
      physInputPhone: new FormControl(),
      physInputFax: new FormControl(),
      physEmail: new FormControl(),*/

      this.setStakeHolder(formData.controls['physProviderBillingNumber'].value);
        this.setFirstName(formData.controls['physFirstName'].value);
        this.setLastName(formData.controls['physLastName'].value);
        this.setPhoneNumber(formData.controls['physInputPhone'].value);
        this.setExtension(formData.controls['physInputExtension'].value);
        this.setExtension(formData.controls['inputExtension'].value);
        this.setFax(formData.controls['physInputFax'].value);
        this.setEmail(formData.controls['physEmail'].value);
    }
    setStakeHolder(stakeholderNo:string)
    {
        this.stakeholderNo = stakeholderNo;
    }

    setFirstName(firstName:Name)
    {
        this.firstName = firstName;
    }

    setLastName(lastName:Name)
    {
        this.lastName = lastName;
    }

    setPhoneNumber(number:Contact)
    {
        this.primaryNumber = number;
    }

    setExtension(extension:Contact)
    {
        this.extension = extension;
    }

    setFax(fax:Contact)
    {
        this.fax = fax;
    }

    setEmail(email:Contact)
    {
        this.email = email;
    }

}

