import { Name } from '../utility/name';
import { Address } from '../utility/address';
import { Contact } from '../utility/contact';
import {FormGroup} from '@angular/forms';
export class Proposed {

    
    facility: string;
    address: Address;
    city: Address;
    province: Address;
    postal: Address;
    primaryNumber: Contact;
    workNumber: Contact;
    extension: Contact;
    fax: Contact;
    email: Contact;
    contact: string;
    contactLastName: string;
    contactFirstName: string;
 




    constructor(options: {
        facility?: string,
        address?: Address,
        city?: Address,
        province?: Address,
        postalCode?: Address,
        dayPhone?: Contact,
        workPhone?: Contact,
        extension?: Contact,
        email?: Contact,
        fax?: Contact,
        contact?: string,
        contactLastName?: string,
        contactFirstName?: string,


    } = {}) {
        this.facility = options.facility;
        this.city = options.city;
        this.province = options.province;
        this.postal = options.postalCode;
        this.primaryNumber = options.dayPhone;
        this.workNumber = options.workPhone;
        this.extension = options.extension;
        this.email = options.email;
        this.fax = options.fax;
        this.contact = options.contact;
        this.contactLastName = options.contactLastName;
        this.contactFirstName = options.contactFirstName;
    }

 
    contrtructProposed(formData: FormGroup)
    {
        /*
             propFaculity: new FormControl(),
      propAddress: new FormControl(),
      propCity: new FormControl(),
      propStateCountry: new FormControl(),
      propCode: new FormControl(),
      propOOC: new FormControl(),
      propLastName: new FormControl(),
      propFirstName: new FormControl(),
      propInputPhone: new FormControl(),
      propInputExtension: new FormControl(),
      propInputFax: new FormControl(),
      propEmail: new FormControl(),
      */
        this.setFacility(formData.controls['propFaculity'].value);
        this.setAddreess(formData.controls['propAddress'].value);
        this.setCity(formData.controls['propCity'].value);
        this.setProvince(formData.controls['propStateCountry'].value);
        this.setPostal(formData.controls['propCode'].value);
        this.setPrimaryNumber(formData.controls['propInputPhone'].value);
        this.setExtension(formData.controls['propInputExtension'].value);
        this.setEmail(formData.controls['propEmail'].value);
        this.setFax(formData.controls['propInputFax'].value);
        this.setRelationship(formData.controls['propOOC'].value);
        this.setContactLastName(formData.controls['propLastName'].value);
        this.setContactFirstName(formData.controls['propFirstName'].value);
   

    }
    setFacility(facility:string)
    {
        this.facility = facility;
    }
    
    setAddreess(address:Address)
    {
        this.address = address;
    }
    setCity(city:Address)
    {
        this.city = city;
    }

    setProvince(province:Address)
    {
        this.province = province;
    }

    setPostal(postal:Address)
    {
        this.postal = postal;
    }

    setPrimaryNumber(primaryNumber:Contact)
    {
        this.primaryNumber = primaryNumber;
    }

    setWorkNumber(workNumber:Contact)
    {
        this.workNumber = workNumber;
    }

    setExtension(extension:Contact)
    {
        this.extension = extension;
    }

    setFax(fax:Contact)
    {
        this.fax = fax;
    }
    setEmail(email:Contact)
    {
        this.email = email;
    }
    setRelationship(relationship:string)
    {
        this.contact = relationship;
    }

    setContactLastName(lastName:string)
    {
        this.contactLastName = lastName;
    }

    setContactFirstName(firstName:string)
    {
        this.contactFirstName = firstName;
    }
   
}

