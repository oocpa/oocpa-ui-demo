import { Name } from '../utility/name';
import { Address } from '../utility/address';
import { Contact } from '../utility/contact';
import {FormGroup} from '@angular/forms';
export class Patient {

    
    healthCardNumber: string;
    versionCode: string;
    firstName: Name;
    intial:Name;
    lastName: Name;
    dateOfBirth: Date;
    sex: string;
    city: Address;
    province: Address;
    postal: Address;
    primaryNumber: Contact;
    workNumber: Contact;
    extension: Contact;
    fax: Contact;
    relationship: string;
    guardLastName: string;
    guardFirstName: string;
 




    constructor(options: {
        healthCardNumber?: string,
        versionCode?: string,
        firstName?: Name,
        intial?: Name,
        lastName?: Name,
        dateOfBirth?: Date,
        sex?: string,
        city?: Address,
        province?: Address,
        postalCode?: Address,
        dayPhone?: Contact,
        workPhone?: Contact,
        extension?: Contact,
        fax?: Contact,
        relationship?: string,
        guardLastName?: string,
        guardFirstName?: string,


    } = {}) {
        this.healthCardNumber = options.healthCardNumber;
        this.versionCode = options.versionCode;
        this.firstName = options.firstName;
        this.intial = options.intial;
        this.lastName = options.lastName;
        this.dateOfBirth = options.dateOfBirth;
        this.city = options.city;
        this.province = options.province;
        this.postal = options.postalCode;
        this.primaryNumber = options.dayPhone;
        this.workNumber = options.workPhone;
        this.extension = options.extension;
        this.fax = options.fax;
        this.relationship = options.relationship;
        this.guardLastName = options.guardLastName;
        this.guardFirstName = options.guardFirstName;
    }

 
    contrtructPaitent(formData: FormGroup)
    {
        /*
         lastNameControl: new FormControl(),

      firstNameControl: new FormControl(),
      intialControl: new FormControl(),
      dobInput: new FormControl(),
      inputSex: new FormControl(),
      healthNumber: new FormControl(),
      versionCode: new FormControl(),
      inputCity: new FormControl(),
      inputProvince: new FormControl(),
      inputPostal: new FormControl(),

      inputHomePhone: new FormControl(),

      inputDayPhone: new FormControl(),
      inputExtension: new FormControl(),
      guardianlastName: new FormControl(),
      guardianfirstName: new FormControl(),
      formComplete: new FormControl(),
      */
        this.setHealthCard(formData.controls['healthNumber'].value);
        this.setVersion(formData.controls['versionCode'].value);
        this.setFirstName(formData.controls['firstNameControl'].value);
        this.setInitalName(formData.controls['intialControl'].value);
        this.setFamilyName(formData.controls['lastNameControl'].value);
        this.setSex(formData.controls['inputSex'].value);
        this.setDOB(formData.controls['dobInput'].value);
        this.setCity(formData.controls['inputCity'].value);
        this.setProvince(formData.controls['inputProvince'].value);
        this.setPostal(formData.controls['inputPostal'].value);
        this.setPrimaryNumber(formData.controls['inputHomePhone'].value);
        this.setWorkNumber(formData.controls['inputDayPhone'].value);
        this.setExtension(formData.controls['inputExtension'].value);
      //  this.setFax(formData.controls['versionCode'].value);
        this.setRelationship(formData.controls['formComplete'].value);
        this.setGuardLastName(formData.controls['guardianlastName'].value);
        this.setGuardFirstName(formData.controls['guardianfirstName'].value);
   

    }
    setHealthCard(healthCard:string)
    {
        this.healthCardNumber = healthCard;
    }
    setVersion(versionCode:string)
    {
        this.versionCode = versionCode;
    }

    setFirstName(firstName: Name)
    {
        this.firstName = firstName;
    
    }
    setInitalName(intial: Name)
    {
        this.intial = intial;
    
    }
    setFamilyName(familyName: Name)
    {
        this.lastName = familyName;
    
    }

    setSex(sex: string)
    {
        this.sex = sex;
    }
    setDOB(DOB:Date)
    {
        this.dateOfBirth = DOB;
    }

    setCity(city:Address)
    {
        this.city = city;
    }

    setProvince(province:Address)
    {
        this.province = province;
    }

    setPostal(postal:Address)
    {
        this.postal = postal;
    }

    setPrimaryNumber(primaryNumber:Contact)
    {
        this.primaryNumber = primaryNumber;
    }

    setWorkNumber(workNumber:Contact)
    {
        this.workNumber = workNumber;
    }

    setExtension(extension:Contact)
    {
        this.extension = extension;
    }

    setFax(fax:Contact)
    {
        this.fax = fax;
    }

    setRelationship(relationship:string)
    {
        this.relationship = relationship;
    }

    setGuardLastName(lastName:string)
    {
        this.guardLastName = lastName;
    }

    setGuardFirstName(firstName:string)
    {
        this.guardFirstName = firstName;
    }
    getPaitent()
    {
        return  this.lastName;
    }
}

