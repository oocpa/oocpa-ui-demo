import {FormGroup} from '@angular/forms';
export class Treatment {

  
    Diagnosis : string;
    Code: string;
    ServiceType: string;
    HosptialStay: string;
    AdmissionDate: string;
    OOCConsult: string;
    SurgeryDate: string;
    ProposedTreatment:string;
    OutOfCountry:string;
    PreviousRequest:string;
    CardiacCare:string;
   

    constructor(options: {
        Diagnosis?: string,
        Code?: string,
        ServiceType?: string,
        HosptialStay?: string,
        AdmissionDate?: string,
        OOCConsult?: string,
        SurgeryDate?: string,
        ProposedTreatment?: string,
        OutOfCountry?: string,
        PreviousRequest?: string,
        CardiacCare?: string,
      
    } = {}) {
        this.Diagnosis = options.Diagnosis;
        this.Code = options.Code;
        this.ServiceType = options.ServiceType;
        this.HosptialStay = options.HosptialStay;
        this.AdmissionDate = options.AdmissionDate;
        this.OOCConsult = options.OOCConsult;
        this.SurgeryDate = options.SurgeryDate;
        this.ProposedTreatment = options.ProposedTreatment;
        this.OutOfCountry = options.OutOfCountry;
        this.PreviousRequest = options.PreviousRequest;
        this.CardiacCare = options.CardiacCare;
    }

    contructTreatment(formData: FormGroup)
    {

        this.setDiagnosis(formData.controls['treatmentInputDiagnosis'].value);
        this.setCode(formData.controls['treatmentInputCode'].value);
        this.setServiceType(formData.controls['treatmentServiceType'].value);
        this.setHosptialStay(formData.controls['treatmentHosptialStay'].value);
        this.setAdmissionDate(formData.controls['treatmentAdmissionDate'].value);
        this.setOOCConsult(formData.controls['treatmentOOCConsult'].value);
        this.setSurgeryDate(formData.controls['treatmentSurgeryDate'].value);
        this.setProposedTreatment(formData.controls['treatmentProposedTreatment'].value);
        this.setOutOfCountry(formData.controls['treatmentOutOfCountry'].value);
        this.setPreviousRequest(formData.controls['treatmentPreviousRequest'].value);
        this.setCardiacCare(formData.controls['treatmentCardiacCare'].value);
    }
    setDiagnosis(Diagnosis:string)
    {
        this.Diagnosis = Diagnosis;
    }

    setCode(Code:string)
    {
        this.Code = Code;
    }

    setServiceType(ServiceType:string)
    {
        this.ServiceType = ServiceType;
    }

    setHosptialStay(HosptialStay:string)
    {
        this.HosptialStay = HosptialStay;
    }

    setAdmissionDate(AdmissionDate:string)
    {
        this.AdmissionDate = AdmissionDate;
    }

    setOOCConsult(OOCConsult:string)
    {
        this.OOCConsult = OOCConsult;
    }

    setSurgeryDate(SurgeryDate:string)
    {
        this.SurgeryDate = SurgeryDate;
    }

    setProposedTreatment(ProposedTreatment:string)
    {
        this.ProposedTreatment = ProposedTreatment;
    }

    setOutOfCountry(OutOfCountry:string)
    {
        this.OutOfCountry = OutOfCountry;
    }

    setPreviousRequest(PreviousRequest:string)
    {
        this.PreviousRequest = PreviousRequest;
    }

    setCardiacCare(PreviousRequest:string)
    {
        this.PreviousRequest = PreviousRequest;
    }

}

