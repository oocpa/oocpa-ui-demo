export class Address {
    city: string;
    province: string;
    street: string;
    postalCode: string;
    line: string;
}
