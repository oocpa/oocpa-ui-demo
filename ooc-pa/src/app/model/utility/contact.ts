export class Contact {
    primaryNumber: string;
    workNumber: string;
    extension: string;
    faxNumber: string;
    email: string;
}
